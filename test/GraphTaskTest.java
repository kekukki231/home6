import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Testklass.
 *
 * @author Kert Kukk
 */
public class GraphTaskTest {

    GraphTask gt = new GraphTask();

    @Test(timeout = 1000)
    public void mstFromGraph1() {
        GraphTask.Graph g = createGraph1();
        GraphTask.Graph mst = g.mst();

        assertTrue(mst.getArc("AB") != null);
        assertTrue(mst.getArc("BA") != null);

        assertTrue(mst.getArc("AC") != null);
        assertTrue(mst.getArc("CA") != null);

        assertTrue(mst.getArc("BD") != null);
        assertTrue(mst.getArc("DB") != null);
    }

    @Test(timeout = 1000)
    public void mstFromGraph2() {
        GraphTask.Graph g = createGraph2();
        GraphTask.Graph mst = g.mst();

        assertTrue(mst.getArc("AB") != null);
        assertTrue(mst.getArc("BA") != null);

        assertTrue(mst.getArc("AF") != null);
        assertTrue(mst.getArc("FA") != null);

        assertTrue(mst.getArc("FC") != null);
        assertTrue(mst.getArc("CF") != null);

        assertTrue(mst.getArc("FE") != null);
        assertTrue(mst.getArc("EF") != null);

        assertTrue(mst.getArc("ED") != null);
        assertTrue(mst.getArc("DE") != null);
    }

    @Test(timeout = 1000)
    public void mstFromGraph3() {
        GraphTask.Graph g = createGraph3();
        GraphTask.Graph mst = g.mst();

        assertTrue(mst.getArc("AC") != null);
        assertTrue(mst.getArc("CA") != null);

        assertTrue(mst.getArc("CD") != null);
        assertTrue(mst.getArc("DC") != null);

        assertTrue(mst.getArc("BD") != null);
        assertTrue(mst.getArc("DB") != null);
    }

    @Test(timeout = 1000)
    public void mstHasCorrectFalls() {
        GraphTask.Graph g = createGraph1();
        GraphTask.Graph mst = g.mst();

        assertEquals(mst.getArc("AB").getFall(), 1);
        assertEquals(mst.getArc("AC").getFall(), 4);
        assertEquals(mst.getArc("DB").getFall(), 2);
    }

    @Test(timeout = 1000)
    public void mstHasCorrectNumberOfVertices() {
        GraphTask.Graph g = createGraph2();
        GraphTask.Graph mst = g.mst();

        List<GraphTask.Vertex> mstVertices = mst.getVertices();
        List<GraphTask.Vertex> gVertices = g.getVertices();

        assertTrue(mstVertices.size() == gVertices.size());
    }

    @Test(timeout = 20000)
    public void mstFromBigGraph() {
        GraphTask.Graph g = gt.new Graph("G");
        g.createRandomSimpleGraph(200, 199, 1, 5);
        g.safestPath("v1", "v10");
    }

    @Test(timeout = 1000)
    public void findSafestPath1() {
        GraphTask.Graph g = createGraph1();
        GraphTask.Graph sp = g.safestPath("D", "C");

        assertTrue(sp.getVertex("A") != null);
        assertTrue(sp.getVertex("B") != null);
        assertTrue(sp.getVertex("C") != null);
        assertTrue(sp.getVertex("D") != null);

        assertTrue(sp.getArc("DB") != null);
        assertTrue(sp.getArc("BD") != null);

        assertTrue(sp.getArc("BA") != null);
        assertTrue(sp.getArc("AB") != null);

        assertTrue(sp.getArc("AC") != null);
        assertTrue(sp.getArc("CA") != null);
    }

    @Test(timeout = 1000)
    public void findSafestPath2() {
        GraphTask.Graph g = createGraph2();
        GraphTask.Graph sp = g.safestPath("B", "E");

        assertTrue(sp.getVertex("B") != null);
        assertTrue(sp.getVertex("A") != null);
        assertTrue(sp.getVertex("F") != null);
        assertTrue(sp.getVertex("E") != null);

        assertTrue(sp.getVertex("C") == null);
        assertTrue(sp.getVertex("D") == null);

        assertTrue(sp.getArc("BA") != null);
        assertTrue(sp.getArc("AB") != null);

        assertTrue(sp.getArc("AF") != null);
        assertTrue(sp.getArc("FA") != null);

        assertTrue(sp.getArc("FE") != null);
        assertTrue(sp.getArc("EF") != null);

        assertTrue(sp.getArc("FC") == null);
        assertTrue(sp.getArc("CF") == null);

        assertTrue(sp.getArc("ED") == null);
        assertTrue(sp.getArc("DE") == null);
    }

    @Test(expected = RuntimeException.class)
    public void minimumFallMustBeGreaterThanZero1() {
        GraphTask.Graph g = gt.new Graph("G");
        int minimumFall = 0;
        g.createRandomSimpleGraph(2, 1, minimumFall, 5);

    }

    @Test(expected = RuntimeException.class)
    public void minimumFallMustBeGreaterThanZero2() {
        GraphTask.Graph g2 = createGraph4();
        g2.safestPath("A", "B");
    }

    @Test(expected = RuntimeException.class)
    public void safestPathStartAndDestinationCantBeEqual() {
        GraphTask.Graph g = createGraph1();
        g.safestPath("A", "A");
    }

    @Test(expected = RuntimeException.class)
    public void startVertexDontExistInGraph() {
        GraphTask.Graph g = createGraph1();
        g.safestPath("wrongVertexName", "B");
    }

    @Test(expected = RuntimeException.class)
    public void destinationVertexDontExistInGraph() {
        GraphTask.Graph g = createGraph1();
        g.safestPath("A", "wrongVertexName");
    }


    private GraphTask.Graph createGraph1() {
        GraphTask.Graph g = gt.new Graph("G");

        GraphTask.Vertex a = g.createVertex("A");
        GraphTask.Vertex b = g.createVertex("B");
        GraphTask.Vertex c = g.createVertex("C");
        GraphTask.Vertex d = g.createVertex("D");

        g.createArc("AB", a, b, 1);
        g.createArc("AD", a, d, 3);
        g.createArc("AC", a, c, 4);

        g.createArc("BA", b, a, 1);
        g.createArc("BD", b, d, 2);

        g.createArc("CA", c, a, 4);
        g.createArc("CD", c, d, 5);

        g.createArc("DB", d, b, 2);
        g.createArc("DA", d, a, 3);
        g.createArc("DC", d, c, 5);
        return g;
    }

    private GraphTask.Graph createGraph2() {
        GraphTask.Graph g = gt.new Graph("G");

        GraphTask.Vertex a = g.createVertex("A");
        GraphTask.Vertex b = g.createVertex("B");
        GraphTask.Vertex c = g.createVertex("C");
        GraphTask.Vertex d = g.createVertex("D");
        GraphTask.Vertex e = g.createVertex("E");
        GraphTask.Vertex f = g.createVertex("F");

        g.createArc("AB", a, b, 2);
        g.createArc("AF", a, f, 1);

        g.createArc("BA", b, a, 2);
        g.createArc("BC", b, c, 3);

        g.createArc("CB", c, b, 3);
        g.createArc("CF", c, f, 1);
        g.createArc("CD", c, d, 3);

        g.createArc("DC", d, c, 3);
        g.createArc("DE", d, e, 2);

        g.createArc("ED", e, d, 2);
        g.createArc("EF", e, f, 2);

        g.createArc("FE", f, e, 2);
        g.createArc("FC", f, c, 1);
        g.createArc("FA", f, a, 1);
        return g;
    }

    private GraphTask.Graph createGraph3() {
        GraphTask.Graph g = gt.new Graph("G");

        GraphTask.Vertex a = g.createVertex("A");
        GraphTask.Vertex b = g.createVertex("B");
        GraphTask.Vertex c = g.createVertex("C");
        GraphTask.Vertex d = g.createVertex("D");

        g.createArc("AC", a, c, 1);
        g.createArc("CA", c, a, 1);

        g.createArc("CD", c, d, 2);
        g.createArc("DC", d, c, 2);

        g.createArc("DB", d, b, 2);
        g.createArc("BD", b, d, 2);

        return g;
    }

    private GraphTask.Graph createGraph4() {
        GraphTask.Graph g = gt.new Graph("G");

        GraphTask.Vertex a = g.createVertex("A");
        GraphTask.Vertex b = g.createVertex("B");

        g.createArc("AB", a, b, 0);
        g.createArc("BA", b, a, 0);

        return g;
    }

}

