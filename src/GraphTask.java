import java.util.*;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Graph g = new Graph("G");

        g.createRandomSimpleGraph(2, 1, 1, 5);
        System.out.println(g);

        Graph sp = g.safestPath("v1", "v2");
        System.out.println("safest path: ---->");
        System.out.println(sp);
    }

    /**
     * Vertex represents one node in graph.
     */
    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;
        private Vertex parent = null;

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

    }

    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        /**
         * Minimum value of fall is 1. So fall can't be zero or negative.
         */
        private int fall = 1;

        Arc(String s, Vertex v, Arc a, int f) {
            id = s;
            target = v;
            next = a;
            fall = f;
        }

        Arc(String s) {
            this(s, null, null, 0);
        }

        Arc(String s, int f) {
            this(s, null, null, f);
        }

        public int getFall() {
            return fall;
        }

        @Override
        public String toString() {
            return id + "." + fall;
        }

    }


    /**
     * Represents graph structure used to model pairwise relations between objects
     */
    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(").");
                    sb.append(a.fall);
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        /**
         * Creates new vertex by id
         *
         * @param vid vertex id
         * @return new vertex
         */
        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        /**
         * Creates new arc by id, start, end destination and fall
         *
         * @param aid  arc id
         * @param from arc start
         * @param to   arc destination
         * @param fall arc fall
         * @return new arc
         */
        public Arc createArc(String aid, Vertex from, Vertex to, int fall) {
            Arc res = new Arc(aid, fall);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Creates new arc by id, start and end destination
         *
         * @param aid  arc id
         * @param from arc start
         * @param to   arc destination
         * @return new arc
         */
        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Gets desired arc
         *
         * @param id desired arc id
         * @return desired arc if found. Otherwise returns null
         */
        public Arc getArc(String id) {
            Vertex v = first;
            Arc ret = null;
            while (v != null) {
                Arc a = v.first;
                while (a != null) {
                    if ((a.id).equals(id)) {
                        ret = a;
                    }
                    a = a.next;
                }
                v = v.next;
            }
            return ret;
        }

        /**
         * Gets desired vertex by id
         *
         * @param id vertex id
         * @return desired vertex if found. Otherwise returns null
         */
        public Vertex getVertex(String id) {
            Vertex v = first;
            Vertex ret = null;
            while (v != null) {
                if ((v.id).equals(id))
                    ret = v;
                v = v.next;
            }
            return ret;
        }

        /**
         * Gets list of vertices
         *
         * @return list of vertices
         */
        public List<Vertex> getVertices() {
            Vertex v = first;
            List<Vertex> vertexes = new ArrayList<Vertex>();
            while (v != null) {
                vertexes.add(v);
                v = v.next;
            }
            return vertexes;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex
         *
         * @param n   number of vertices added to this graph
         * @param min minimum value of randomly generated fall
         * @param max maximum value of randomly generated fall
         */
        public void createRandomTree(int n, int min, int max) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int rnd = randInt(min, max);
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i], rnd);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr], rnd);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];

            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j] = a.fall;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges
         *
         * @param n   number of vertices
         * @param m   number of edges
         * @param min minimum value of randomly generated fall
         * @param max maximum value of randomly generated fall
         */
        public void createRandomSimpleGraph(int n, int m, int min, int max) {
            if (min < 1)
                throw new RuntimeException("Minimum value must be equal or greater than 1!");
            if (n <= 0)
                return;
            if (n > 10000)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n, min, max);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Generates random number in given gap
         *
         * @param min minimum value of randomly generated number
         * @param max maximum value of randomly generated number
         * @return random integer
         */
        public int randInt(int min, int max) {
            return min + (int) (Math.random() * ((max - min) + 1));
        }

        /**
         * Creates minimal spanning tree of this graph
         *
         * @return minimal spanning tree
         */
        public Graph mst() {
            Graph g = new Graph("G");
            int[][] matrix = createAdjMatrix();

            List<Vertex> vertices = getVertices();
            List<Integer> usedRow = new ArrayList<>();

            int min = Integer.MAX_VALUE;
            int rowNr = -1;
            int colNr = -1;
            int cycleCount = 1;

            for (int i = 0; i < matrix.length; i++) {

                if (g.getVertices().isEmpty()) {
                    g.createVertex(vertices.get(0).id);
                    usedRow.add(i);
                }

                if (!usedRow.contains(i)) {
                    continue;
                }

                for (int j = 0; j < matrix[i].length; j++) {
                    if (usedRow.contains(j)) {
                        continue;
                    }
                    int current = matrix[i][j];

                    if (current != 0 && current < min) {
                        min = current;
                        rowNr = j;
                        colNr = i;
                    }
                }

                if (cycleCount == matrix.length) {
                    break;
                } else if (cycleCount == usedRow.size()) {
                    if (min < 1 || rowNr < 0 || colNr < 0) {
                        throw new RuntimeException("Minimum value must be equal or greater than 1!");
                    }
                    g.createVertex(vertices.get(rowNr).id);

                    String from = vertices.get(colNr).id;
                    String to = vertices.get(rowNr).id;

                    g.createArc(from + to, g.getVertex(from), g.getVertex(to), min);
                    g.createArc(to + from, g.getVertex(to), g.getVertex(from), min);
                    usedRow.add(rowNr);

                    min = Integer.MAX_VALUE;

                    rowNr = -1;
                    colNr = -1;
                    i = -1;
                    cycleCount = 1;
                } else {
                    cycleCount++;
                }
            }

            return g;
        }

        /**
         * Finds safest path between given vertex names
         *
         * @param from starting point of the path
         * @param to   destination point of the path
         * @return safest path between given vertices
         */
        public Graph safestPath(String from, String to) {

            List<Vertex> vertexes = getVertices();
            checkVertexesExistence(from, to, vertexes);
            Graph mst = mst();

            Graph g = new Graph("G");

            Vertex tempVert = mst.getVertex(from);
            Queue<Vertex> queue = new LinkedList<>();
            List<Vertex> viewedVertexes = new ArrayList<>();
            viewedVertexes.add(tempVert);

            while (tempVert.next != null || tempVert.first != null) {

                Arc a = tempVert.first;
                while (a != null) {
                    if (viewedVertexes.contains(a.target)) {
                        a = a.next;
                        continue;
                    }
                    a.target.parent = tempVert;
                    queue.add(a.target);
                    viewedVertexes.add(a.target);
                    a = a.next;
                }

                if (queue.isEmpty()) {
                    break;
                }
                tempVert = queue.remove();

                if (tempVert.id.equals(to)) {

                    while (tempVert.parent != null) {
                        if (g.getVertex(tempVert.id) == null) {
                            g.createVertex(tempVert.id);
                        }
                        if (g.getVertex(tempVert.parent.id) == null) {
                            g.createVertex(tempVert.parent.id);
                        }
                        Arc arc = tempVert.first;
                        int fall = 0;
                        while (arc != null) {
                            if (arc.target.id == tempVert.parent.id) {
                                fall = arc.fall;
                                break;
                            }
                            arc = arc.next;
                        }
                        g.createArc(tempVert.id + tempVert.parent.id, g.getVertex(tempVert.id), g.getVertex(tempVert.parent.id), fall);
                        g.createArc(tempVert.parent.id + tempVert.id, g.getVertex(tempVert.parent.id), g.getVertex(tempVert.id), fall);
                        tempVert = tempVert.parent;
                    }

                    if (tempVert.parent == null && g.getVertices().contains(tempVert)) {
                        g.createVertex(tempVert.id);
                    }
                }
            }

            return g;
        }

        /**
         * Checks if vertices exists in given list
         *
         * @param from     start vertex
         * @param to       destination vertex
         * @param vertices list of vertices
         */
        private void checkVertexesExistence(String from, String to, List<Vertex> vertices) {
            if (from == to) {
                throw new RuntimeException("Vertex from and to (" + from + ") are equivalent. So there is no path!");
            }
            StringTokenizer st = new StringTokenizer(vertices.toString(), "[,] ");
            boolean f = false, t = false;
            while (st.hasMoreTokens()) {
                String temp = st.nextToken();
                if (temp.equals(from)) {
                    f = true;
                } else if (temp.equals(to)) {
                    t = true;
                }
                if (f == true && t == true) {
                    break;
                }
            }
            if (f != true) {
                throw new RuntimeException("Vertex '" + from + "' doesn't exist in this graph!");
            } else if (t != true) {
                throw new RuntimeException("Vertex '" + to + "' doesn't exist in this graph!");
            }
        }

    }
} 

